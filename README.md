![Alt text](header.jpg)

# RentASnow

RentASnow - ICT151 : application PHP/MySQL utilisant une architecture MVC procédurale

## Getting Started

*créez un repertoire 151/rentasnow dans C:/wamp/www

### Prerequisites

* Apache 2
* PHP 5
* MySQ5 5


### Installing

1. Base de données installée (script snows2017.sql exécuté dans MySQL après avoir au préalable créé la base de données surfs
2. Création d'un répertoire dans wwww 151/rentasnow
3. Créez l'architecture suivant

```
rentasnow/
          contenu
          controleur
          modele
          vue
```
4. Copiez le template contenu dans le zip dans le répertoire contenu

A présent vous êtes prêts pour démarrer le développement du projet


## Étapes de développement

Ces différentes étapes sont réparties par semaine mais peuvent varier d'une classe à l'autre et des problèmes rencontrés lors des tests unitaires.

Le détail de ces étapes est donné dans le Wiki : [Etapes de développement](https://bitbucket.org/pba_cpnv/151_2017/wiki/Etapes%20de%20d%C3%A9veloppement)


## Running the tests

A compléter