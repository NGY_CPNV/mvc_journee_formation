<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:10
 */

require "modele/modele.php";

// Affichage de la page de l'accueil
function accueil()
{
  require "vue/vue_accueil.php";
}

function erreur($e)
{
  $_SESSION['erreur']=$e;
  require "vue/vue_erreur.php";
}

// ----------------- Fonctions en lien avec les snows ---------------------

function snows()
{
  $resultats=getSnows(); // pour récupérer les données des snows dans la BD
  require 'vue/vue_snows.php';
}

function addSnow()
{
  //Test si le formulaire est rempli
  if (isset($_POST['fIDSnow']))
    ajoutSnow($_POST);  // Envoi vers le modèle des contenus issus du formulaire
  require 'vue/vue_add_snow.php';
}

function delSnow()
{
  if (isset($_GET['ID']))
  {
    supprSnow($_GET['ID']);
    require "vue/vue_del_snow.php";
  }
  else
    require "vue/vue_snows.php";
}


function updSnow()
{
  if (isset($_GET['ID']))
  {
    // Si j'ai un ID --> Récupérer les données du snow choisi
    $resultat = getASnow($_GET['ID']);
    require "vue/vue_upd_snow.php";
    exit();
  }
  else
  {
    updateSnow($_POST);
    $resultats=getSnows();
    require "vue/vue_snows.php";
  }

}

// --------------------- Fonction utiliateur --------------------------

function login()
{
  if (isset ($_POST['fLogin']) && isset ($_POST['fPass']))
  {
    $resultats = getLogin($_POST);
    require "vue/vue_user_login.php";
  }
  else
  {
    // détruit la session de la personne connectée après appuyé sur Logout
    if (isset($_SESSION['login'])) {
      session_destroy();
      require "vue/vue_accueil.php";
    }
    else
      require "vue/vue_user_login.php";
  }
}
