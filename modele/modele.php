<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:15
 */

// ---------------------------------------------
// getBD()
// Fonction : connexion avec le serveur : instancie et renvoie l'objet PDO
// Sortie : $connexion

function getBD()
{
  // connexion au server de BD MySQL et à la BD
  $connexion = new PDO('mysql:host=localhost; dbname=snows', 'nicolas.glassey', '1qa2ws3eD!');
  // permet d'avoir plus de détails sur les erreurs retournées
  $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $connexion;
}

// -----------------------------------------------------
// Fonctions liées aux snows

// getSnows()
// Fonction : Récupérer les données des snows
// Sortie : $resultats

function getSnows()
{
  // Connexion à la BD et au serveur
  $connexion = getBD();

  // Création de la string pour la requête
  $requete = "SELECT * FROM tblsurfs ORDER BY idsurf;";
  // Exécution de la requête
  $resultats = $connexion->query($requete);
  return $resultats;
}

// ------------------------ Sélection d'un snow --------------------

function getASnow($ID)
{
  $connexion= getBD();
  $requete= "SELECT * FROM tblsurfs WHERE idsurf='".$ID."';";
  $resultat = $connexion->query($requete);
  return $resultat;
}


// ------------------------ Ajout d'un snow ------------------------

function ajoutSnow()
{
  // Connexion à la BD et au serveur
  $connexion = getBD();
  extract($_POST);
  // test si la valeur est déjà existante
  try
  {
    // requête pour tester l'ID du snow existe déjà
    $reqID ="SELECT idsurf FROM tblsurfs WHERE idsurf ='".$fIDSnow."';";
    $resID =$connexion->query($reqID);
    $ligne= $resID->fetch();

    // Ajout de snow si pas de doublon --> fetch() ne renvoie rien
    if (empty($ligne['idsurf']))
    {
      // pas de doublon --> OK, on peut insérer les données
      $reqIns="INSERT INTO tblsurfs VALUES ('".$fIDSnow."','".$fMarque."','".$fBoot."','".$fType."',".$fDispo.",'')";
      $connexion->exec($reqIns);
    }
    else
    {
      // doublon --> erreur
      throw new Exception("Erreur : doublon sur la clé primaire IDSurf");
    }

  }
  catch (Exception $e)
  {
    trigger_error($e->getMessage(), E_USER_ERROR);
  }
}

// ------------------ Suppression de snows -----------------

function supprSnow($ID)
{
  $connexion = getBD();

  $req_suppr = "DELETE FROM tblsurfs WHERE idsurf='".$ID."'";
  $connexion->exec($req_suppr);
}

// ------------------ Modification de snow -----------------

function updateSnow($post)
{
  $connexion = getBD();

  extract($post);  // Transfert des données du POST dans des variables
  // Repère la ligne
  $requete = "UPDATE tblsurfs SET marque='".$fMarque."',boots='".$fBoot."', type='".$fType."',disponibilite=".$fDispo." WHERE idsurf='".$fID."';";
  //$requete = "UPDATE tblsurfs SET marque='".$fMarque."',boots='".$fBoot."', type='".$fType."',disponibilite=".$fDispo." WHERE idsurf='B126';";

  $connexion->exec($requete);

}


// -----------------------------------------------------
// Fonctions liées aux utilisateurs

// ---------------------------------------------------
// getLogin()
// Fonction : Récupérer les données du login de la BD
// Sortie : $resultats

function getLogin($post)
{
  // connexion à la BD snows
  $connexion = getBD();

  // Requête pour sélectionner la personne loguée
  if ($post['fUserType'] == 'Client')
  {
    $requete = "SELECT * FROM tblclients WHERE login= '".$post['fLogin']."' AND passwd='".$post['fPass']."';";
  }
  else
  {
    $requete = "SELECT * FROM tblvendeurs WHERE login= '".$post['fLogin']."' AND passwd='".$post['fPass']."';";
  }

  // Exécution de la requête et renvoi des résultats
  $resultats = $connexion->query($requete);
  return $resultats;
}